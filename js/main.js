document.addEventListener("DOMContentLoaded",function(){
    $(".button-collapse").sideNav(),
    $(".parallax").parallax(),
    $(".slider").slider({indicators:!1,height:600}),
    $(".scrollspy").scrollSpy(),
    $("input#input_text, textarea#textarea1").characterCounter(),
    $(".tooltipped").tooltip({delay:50})
});