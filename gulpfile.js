var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    uglifycss = require('gulp-uglifycss'),
    concat = require  ('gulp-concat'),
    htmlmin = require ('gulp-htmlmin'),
    rename = require ('gulp-rename')
    ;

//script task
gulp.task('scripts',function(){
    gulp.src('../*.js').pipe(uglify()).pipe(gulp.dest('minjs'));
    console.log('comprimiendo scripts');

});
//sass task
gulp.task('styles', function() {
    //object options
    return sass('../*.scss',{style: 'compressed'})
        .on('error', sass.logError)
        .pipe(gulp.dest('mincss'))
});

//css task
gulp.task('css',function () {
    gulp.src('../*.css')
             .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(gulp.dest('mincss'));
    console.log('comprimiendo css');
});

// Watch taskt , watches Js
gulp.task('watch', function(){
    gulp.watch('../*.js', ['scripts']);
    console.log('Algo a cambiado');
});

//callback con array de tareas
gulp.task('default', ['scripts','css','watch']);


